﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modding;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sonic
{
    public class Sonic : Mod
    {
        public GameObject smallGeoPrefab;
        public GameObject mediumGeoPrefab;
        public GameObject largeGeoPrefab;
        public int GEO_VALUE_MEDIUM = 5;
        public int GEO_VALUE_LARGE = 25;
        private GameObject gameObject;

        public override string GetVersion()
        {
            return "Mania";
        }

        public override void Initialize()
        {
            ModHooks.Instance.TakeHealthHook += HealthTaken;
            UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoad;
            ModHooks.Instance.GetPlayerIntHook += IntGet;
            Resources.LoadAll<GameObject>("");
            foreach (var i in Resources.FindObjectsOfTypeAll<GameObject>())
            {
                switch(i.name)
                {
                    case "Geo Large":
                        largeGeoPrefab = i;
                        break;
                    case "Geo Med":
                        mediumGeoPrefab = i;
                        break;
                    case "Geo Small":
                        smallGeoPrefab = i;
                        break;
                }
            }
            Log("init");
        }

        private int IntGet(string intName)
        {
            switch(intName)
            {
                case "maxHealth":
                    return 1;
                case "maxHealthBase":
                    return 1;
                default:
                    return PlayerData.instance.GetIntInternal(intName);
            }
        }

        private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
        {
            gameObject = HeroController.instance.gameObject;
        }

        private int HealthTaken(int damage)
        {
            if (PlayerData.instance.geo <= 0)
            {
                return 100;
            }
            else
            {
                SpawnGeo(PlayerData.instance.geo < 1000 ? PlayerData.instance.geo : 1000);
                HeroController.instance.TakeGeo(PlayerData.instance.geo);
                return 0;
            }
        }

        public void SpawnGeo(int amount)
        {
            if (smallGeoPrefab == null || mediumGeoPrefab == null || largeGeoPrefab == null)
            {
                Log("REEE");
            }

            if (amount > 0)
            {
                if (smallGeoPrefab != null && mediumGeoPrefab != null && largeGeoPrefab != null)
                {
                    System.Random random = new System.Random();

                    int smallNum = random.Next(0, amount / 10);
                    amount -= smallNum;
                    int largeNum = random.Next(amount / (GEO_VALUE_LARGE * 2), amount / GEO_VALUE_LARGE + 1);
                    amount -= largeNum * GEO_VALUE_LARGE;
                    int medNum = amount / GEO_VALUE_MEDIUM;
                    amount -= medNum * 5;
                    smallNum += amount;

                    FlingUtils.SpawnAndFling(new FlingUtils.Config
                    {
                        Prefab = smallGeoPrefab,
                        AmountMin = smallNum,
                        AmountMax = smallNum,
                        SpeedMin = 15f,
                        SpeedMax = 30f,
                        AngleMin = 80f,
                        AngleMax = 115f
                    }, gameObject.transform, new Vector3(0f, 0f, 0f));
                    FlingUtils.SpawnAndFling(new FlingUtils.Config
                    {
                        Prefab = mediumGeoPrefab,
                        AmountMin = medNum,
                        AmountMax = medNum,
                        SpeedMin = 15f,
                        SpeedMax = 30f,
                        AngleMin = 80f,
                        AngleMax = 115f
                    }, gameObject.transform, new Vector3(0f, 0f, 0f));
                    FlingUtils.SpawnAndFling(new FlingUtils.Config
                    {
                        Prefab = largeGeoPrefab,
                        AmountMin = largeNum,
                        AmountMax = largeNum,
                        SpeedMin = 15f,
                        SpeedMax = 30f,
                        AngleMin = 80f,
                        AngleMax = 115f
                    }, gameObject.transform, new Vector3(0f, 0f, 0f));
                }
                else
                {
                    HeroController.instance.AddGeo(amount);
                }
            }
        }

    }
}
